/**
 * Created by dse on 1/24/15.
 */
public class WorkerWithRunnable {

    public static void main(String[] args) {
        Runnable runnable = new Runnable () {
            public void run() {
                long sum = 0;
                for (int i = 0; i < 10000000; i++) {
                    sum = sum + i; // do some work

                    // every n iterations, print an update
                    // (a bitwise & would be faster -- mod is slow)
                    if (i % 1000000 == 0) {
                        Thread running = Thread.currentThread();
                        System.out.println(running.getName() + " " + i);
                    }
                }
            }
        };

        System.out.println("Starting...");
        Thread a = new Thread(runnable);
        Thread b = new Thread(runnable);
        a.start();
        b.start();

        // The current running thread (executing main()) blocks
        // until both workers have finished
        try {
            a.join();
            b.join();
        } catch (Exception ignored) {
        }

        System.out.println("All done");
    }
}
