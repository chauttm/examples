// Simple class that encapsulates 2 numbers 

class Pair {
    private int a, b;

    public Pair() {
        a = 0;
        b = 0;
    }

    // Returns the sum of a and b. (reader)
    public int sum() {
        return(a+b);
    }

    // Increments both a and b. (writer)
    public synchronized void inc() {
        a++;
        b++;
    }
} 