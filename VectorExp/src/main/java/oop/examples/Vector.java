package oop.examples;

public class Vector {
	int x;
	int y;

    public Vector() {   }

	public Vector(int _x, int _y) {
		x = _x;
		y = _y;
	}
	
	public String toString() {
		return  "(" + x + "," + y + ")";
	}
	
	public Vector add(Vector other) {
		Vector sum = new Vector(x + other.x , 
								y + other.y );
		return sum;
	}

    public Vector subtract(Vector other) {
        return null;
    }
	
	public boolean equals(Object other) {
		Vector v = (Vector)other;
        // if (x == v.x && y == v.y) return true;
        // else return false;
		return (x == v.x && y == v.y);
	}

    public double length() {
        return (Math.sqrt(x*x + y*y));
    }
}