package oop.examples;

/**
 * Created by dse on 11/15/14.
 */
public class VectorDemo {
    public static void main( String[] args )
    {
        Vector v = new Vector();
        v.x = 4;
        v.y = 3;

        Vector v2 = new Vector();
        v2.x = 5;
        v2.y = 6;

        System.out.println("Vector v: " + v.x + "," + v.y);
        System.out.println("Vector v.toString(): " + v.toString() );
        System.out.println("Vector v2.toString(): " + v2 ); // o day v2 se duoc tu dong chuyen thanh v2.toString()

        System.out.println(v + " + " + v2 + " = " + v.add(v2));

        //System.out.println(v.dotProduct(v2));   // can in ra gia tri 38 (la 4x5+3x6)
        System.out.println("Length of v: " + v.length());  //v.length() se tu dong chuyen thanh String

        Vector v3 = new Vector(1,1);
    }
}
