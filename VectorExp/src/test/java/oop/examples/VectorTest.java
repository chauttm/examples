package oop.examples;

import org.junit.*;
import static org.junit.Assert.*;

import oop.examples.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: chauttm
 * Date: 9/18/13
 * Time: 1:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class VectorTest {
    @Test
    public void testToString() {
        Vector v = new Vector(2,3);
		assertEquals("(2,3)", v.toString());
    }
	
	@Test
	public void testEquals() {
        Vector v = new Vector(2,3);
		Vector u = new Vector(2,3);
		Vector o = new Vector(1,5);
		
		assertEquals(u,v);
		assertNotSame(u,o);
    }

	@Test
    public void testAdd() {
        Vector v = new Vector(2,3);
        Vector u = new Vector(4,5);

        Vector sum = v.add(u);

        assertEquals(6, sum.x);
        assertEquals(8, sum.y);
    }

    @Test
    public void testSubtract() {
        Vector v = new Vector(1,3);
        Vector u = new Vector(4,5);

        Vector difference = u.subtract(v);

        assertEquals(3, difference.x);
        assertEquals(4, difference.y);
    }
}



