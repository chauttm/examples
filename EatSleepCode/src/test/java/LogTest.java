import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by dse on 12/13/2014.
 */
public class LogTest {

    @Test
     public void testStreakStartsAt() {
        Log log = new Log("ECS");
        assertFalse(log.streakStartsAt(0));
        assertTrue(log.streakStartsAt(1));
        assertFalse(log.streakStartsAt(2));
    }

    @Test
    public void testStreakEndsAt() {
        Log log = new Log("ECS");

        assertFalse(log.streakEndsAt(0));
        assertTrue(log.streakEndsAt(1));
        assertFalse(log.streakEndsAt(2));
    }

    @Test
    public void testLongestStreak_simpleCase() {
        Log log = new Log("ECCCCS");
        assertEquals(4, log.getLongestStreak());
    }

    @Test
    public void testLongestStreak_MinimalStreak() {
        Log log = new Log("ECS");
        assertEquals(1, log.getLongestStreak());
    }

    @Test
    public void testLongestStreak_MinimalStreak_2() {
        Log log = new Log("C");
        assertEquals(1, log.getLongestStreak());
    }

    @Test
    public void testLongestStreak_MoreThanOneStreak() {
        Log log = new Log("ECCCCSESESCCE");
        assertEquals(4, log.getLongestStreak());
    }
}
