/**
 * Created by dse on 12/13/2014.
 */
public class Log {
    private int longestStreak;
    private String log;

    public Log(String s) {
        log = s;
        longestStreak = 0;
        StringBuilder a =
        int start = 0; // start of the current streak
        for (int i = 0; i < s.length(); i++) {
            if (streakStartsAt(i)) {
                start = i;
            }
            if (streakEndsAt(i)) {
                int streakLength = i - start + 1;
                if (longestStreak < streakLength) {
                    longestStreak = streakLength;
                }
            }
        }
    }

    public String toString() {
        return log;
    }

    boolean streakEndsAt(int index) {
        if (log.charAt(index) == 'C' && index == log.length()-1) {
            return true;
        }
        if (log.charAt(index) == 'C' && log.charAt(index+1) != 'C') {
            return true;
        }
        return false;
    }

    boolean streakStartsAt(int index) {
        if (log.charAt(index) == 'C' && index == 0) {
            return true;
        }
        if (log.charAt(index) == 'C'
                && log.charAt(index-1) != 'C') {
            return true;
        }
        return false;
    }

    public int getLongestStreak() {
        return this.longestStreak;
    }
}
