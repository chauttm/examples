import java.util.Scanner;

/**
 * Created by dse on 12/13/2014.
 */
public class EatSleepCode {
    static int N;
    static String[] dailyLog;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        N = input.nextInt();
        input.nextLine(); // skip the rest of the line
        dailyLog = new String[N];
        for (int i = 0; i < N; i++) {
            dailyLog[i] = input.nextLine();
        }

        int X = calculateLongestStreakInADay();
        int Y = calculateLongestStreakInAllDays();

        System.out.print(X + " " + Y);
    }

    private static int calculateLongestStreakInADay() {
        int max = 0;
        for (int i = 0; i < N; i++) {
            // find longest streak of day i
            int longestStreakOfDay = longestStreak(dailyLog[i]);
            if (max < longestStreakOfDay) {
                max = longestStreakOfDay;
            }
        }
        return max;
    }

    private static int longestStreak(String log) {
        int max = 0;
        int start = 0; // start of the current streak
        int end; // end of the current streak;
        for (int i = 0; i < log.length(); i++) {
            if (streakStartsAt(i, log)) {
                start = i;
            } else if (streakEndsAt(i, log)) {
                end = i;
                int streakLength = end - start + 1;
                if (max < streakLength) {
                    max = streakLength;
                }
            }
        }

        return max;
    }

    private static boolean streakEndsAt(int index, String log) {
        if (log.charAt(index) == 'C' && index == log.length()-1) {
            return true;
        }
        if (log.charAt(index) == 'C' && log.charAt(index+1) != 'C') {
            return true;
        }
        return false;
    }

    private static boolean streakStartsAt(int index, String log) {
        if (log.charAt(index) == 'C' && index == 0) {
            return true;
        }
        if (log.charAt(index) == 'C'
                && log.charAt(index-1) != 'C') {
            return true;
        }
        return false;
    }

    private static int calculateLongestStreakInAllDays() {
        String allLogs = "";

        for (int i = 0; i < N; i++) {
            allLogs = allLogs + dailyLog[i];
        }
        return longestStreak(allLogs);

    }
}
