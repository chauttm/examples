import java.util.Scanner;

/**
 * Created by dse on 12/13/2014.
 */
public class EatSleepCodeOOP {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        input.nextLine(); // skip the rest of the line
        Log[] dailyLog= new Log[N];
        for (int i = 0; i < N; i++) {
            dailyLog[i] = new Log(input.nextLine());
        }

        int X = calculateLongestStreakInADay(dailyLog);
        int Y = calculateLongestStreakInAllDays(dailyLog);

        System.out.print(X + " " + Y);
    }

    private static int calculateLongestStreakInAllDays(Log[] dailyLog) {
        String allLogs = "";

        for (int i = 0; i < dailyLog.length; i++) {
            allLogs = allLogs + dailyLog[i];
        }
        Log allDaysLog = new Log(allLogs);
        return allDaysLog.getLongestStreak();
    }

    private static int calculateLongestStreakInADay(Log[] dailyLog) {
        int max = 0;
        for (int i = 0; i < dailyLog.length; i++) {
            int longestStreakOfDay = dailyLog[i].getLongestStreak();
            if (max < longestStreakOfDay) {
                max = longestStreakOfDay;
            }
        }
        return max;
    }

}
