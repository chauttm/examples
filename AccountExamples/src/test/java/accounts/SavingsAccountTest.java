package accounts;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dse on 12/6/2014.
 */
public class SavingsAccountTest {
    @Test
    public void testAccountHasAnInterestRate() {
        SavingsAccount account =
                new SavingsAccount("124", "John", 100.0, 0.1);
        assertEquals(0.1, account.getInterestRate(),0.01);
    }

    @Test
    public void testBalanceIncreasesAccordingToInterestRate() {
        SavingsAccount account =
                new SavingsAccount("124", "John", 100.0, 0.1);

        account.processAnnualInterest();

        assertEquals(110.0, account.getBalance(), 0.01);
    }

    @Test
    public void testAccountHasABalance() {
        SavingsAccount account =
                new SavingsAccount("123", "John", 100.0, 0.1);

        double accountBalance = account.getBalance();
        assertEquals(100.0, accountBalance, 0.01);
    }

    @Test
    public void testDeposit() {
        SavingsAccount account =
                new SavingsAccount("123", "John", 100.0, 0.1);

        account.deposit(10.0);

        assertEquals(110.0,  account.getBalance(), 0.01);
    }

    @Test
    public void testCanWithdrawUpToBalance() {
        SavingsAccount account =
                new SavingsAccount("123", "John", 100.0, 0.1);

        account.withdraw(10.0);

        assertEquals(90.0,  account.getBalance(), 0.01);
    }

    @Test
    public void testCannotWithdrawMoreThanBalance() {
        SavingsAccount account =
                new SavingsAccount("123", "John", 100.0, 0.1);

        account.withdraw(100.1);

        assertEquals(100.0,  account.getBalance(), 0.01);
    }
}
