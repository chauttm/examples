package accounts;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by dse on 12/6/2014.
 */
public class CheckingAccountTest {
    @Test
    public void testAccountHasABalance() {
        CheckingAccount account =
                new CheckingAccount("123", "John", 100.0);

        double accountBalance = account.getBalance();
        assertEquals(100.0, accountBalance, 0.01);
    }

    @Test
    public void testDeposit() {
        CheckingAccount account =
                new CheckingAccount("123", "John", 100.0);

        account.deposit(10.0);

        assertEquals(110.0,  account.getBalance(), 0.01);
    }
}
