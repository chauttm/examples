package accounts;

/**
 * Created by dse on 1/23/15.
 */
public class InvalidBalanceException extends Exception {
    private double balance;
    private double limit;

    public InvalidBalanceException(double balance, double limit) {
        this.balance = balance;
        this.limit = limit;
    }

    public double getBalance() {
        return balance;
    }

    public double getLimit() {
        return limit;
    }
}
