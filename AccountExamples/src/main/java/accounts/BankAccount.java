package accounts;

/**
 * Created by dse on 12/6/2014.
 */
public class BankAccount {
    final String accountNumber;
    final String owner;
    private double balance;

    final static double LIMIT = 50;

    //public BankAccount() {};

    public BankAccount(String number, String owner,
                          double initialBalance) throws InvalidBalanceException {
        //System.out.println("BankAccount constructor" + number);
        if (initialBalance <= LIMIT) {
            throw new InvalidBalanceException(initialBalance, LIMIT);
        }
        balance = initialBalance;
        accountNumber = number;
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        if (amount < 0) {
            throw new InvalidAmountException();
        }
        balance += amount;
        log("deposit " + amount + "to account" + accountNumber);

    }

    private void log(String s) {
        System.out.println(s);
    }

    public void withdraw(double amount) {
        if (amount < 0) {
            throw new InvalidAmountException();
        }
        balance -= amount;
        log("withdraw " + amount + "from account" + accountNumber);

    }

    public String toString() {
        return "Account number: " + accountNumber +
                ", Owner: " + owner +
                ", Balance: " + getBalance();
    }

    public void endYearProcess() {
        System.out.println("End year status: " + toString());
    }
}
