package accounts;

/**
 * Created by dse on 12/6/2014.
 */
//CheckingAccount: không có lãi suất,
// cho rút tiền quá số dư hiện tại.
public class CheckingAccount extends BankAccount{

    public CheckingAccount(String number, String owner,
                           double initialBalance) throws InvalidBalanceException {
        super(number, owner, initialBalance);
        //System.out.println("CheckingAccount constructor" + number);
    }
}
