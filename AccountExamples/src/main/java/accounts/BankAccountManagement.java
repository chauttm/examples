package accounts;

/**
 * Created by dse on 12/6/2014.
 */
public class BankAccountManagement {
    public static void main(String[] args) {
        try {
            CheckingAccount c1 = new CheckingAccount("123", "John", 100.0);
            CheckingAccount c2 = new CheckingAccount("124", "Mary", 10.0);

            SavingsAccount s1 = new SavingsAccount("223", "John", 90.0, 0.1);
            SavingsAccount s2 = new SavingsAccount("224", "Sabine", 1000.0, 0.1);


               BankAccount[] accounts = new BankAccount[10];

            accounts[0] = c1;
            accounts[1] = c2;
            accounts[2] = s1;
            accounts[3] = s2;
            int numberOfAccounts = 4;

            for (int i = 0; i < numberOfAccounts; i++) {
                accounts[i].endYearProcess();
            }
        }
        catch (InvalidBalanceException e) {
            System.out.println("Invalid balance: (" + e.getBalance() +
                    "), must greater than " + e.getLimit());
        }
        catch (InvalidAmountException e) {
            System.out.print("Invalid amount, must be positive");
        }

        System.out.print("Program continues");
    }
}
