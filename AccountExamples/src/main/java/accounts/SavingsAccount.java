package accounts;

/**
 * Created by dse on 12/6/2014.
 */
//CheckingAccount: không có lãi suất,
// cho rút tiền quá số dư hiện tại.
public class SavingsAccount  extends BankAccount{


    private static double interestRate;

    public SavingsAccount(String number, String owner,
                          double initialBalance,
                          double rate) throws InvalidBalanceException {
        super(number, owner, initialBalance);
        //System.out.println("SavingsAccount constructor" + number);

        interestRate = rate;
    }

    public void withdraw(double amount) {
        if (amount > getBalance()) {
            return;
        }
        super.withdraw(amount);
    }

    public double getInterestRate() {
        return interestRate;
    }

    public String toString() {
        return super.toString() +
                "_" + interestRate;
    }


    public void processAnnualInterest() {
        //tinh tien lai
        double interest = getBalance() * interestRate;
        // nhap goc
        deposit(interest);
    }
}
