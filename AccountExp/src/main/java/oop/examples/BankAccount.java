package oop.examples;

/**
 * Created by dse on 11/22/14.
 */
public class BankAccount {

    private String accountNumber;
    private String owner;
    private int balance;

    public BankAccount(String _accountNumber,
                       String owner, int balance) {
        this.accountNumber = _accountNumber;
        this.owner = owner;
        this.balance = balance;
    }

    public boolean equals(Object other) {
        BankAccount otherAccount = (BankAccount)other;

        if (accountNumber.equals(otherAccount.accountNumber)) {
            return true;
        }
        return false;
    }

    public String getAccountNumber() {


        return accountNumber;
    }

    public String getOwner() {
        return owner;
    }

    public int getBalance() {
        return balance;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {

    }

    public void transfer(int amount, BankAccount receiverAccount) {

    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String toString() {
        return ("Tai khoan cua "
                + getOwner() +
                ": so tai khoan:" + this.getAccountNumber() +
                ", so du: " + getBalance());
    }
}
