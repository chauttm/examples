package oop.examples;

import oop.examples.BankAccount;

/**
 * Created by dse on 11/22/14.
 */
public class BankAccountDemo {
    public static void main(String args[]) {
        BankAccount account1 = new BankAccount("1", "A", 10);
        BankAccount account2 = new BankAccount("2", "B", 20);

        System.out.println(account1.toString());

        account1.setOwner("AA");

        System.out.println(account1.toString());
    }
}
