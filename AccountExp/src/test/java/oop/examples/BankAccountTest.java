package oop.examples;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by dse on 11/22/14.
 */
public class BankAccountTest {
    @Test
    public void testBankAccount () {
        BankAccount account =
                new BankAccount("123","Nguyen Van A",10);
        assertEquals("123", account.getAccountNumber());
        assertEquals("Nguyen Van A", account.getOwner());
        assertEquals(10, account.getBalance());
    }

    @Test
    public void testDeposit () {
        BankAccount account =
                new BankAccount("123","Nguyen Van A",10);

        account.deposit(20);
        assertEquals(30, account.getBalance());
    }

    @Test
    public void testWithdraw_AmountShouldDecrease () {
        BankAccount account =
                new BankAccount("123","Nguyen Van A",100);

        account.withdraw(20);
        assertEquals(80, account.getBalance());
    }

    @Test
     public void testWithdraw_ShouldNotAllowWithdrawMoreThanBalance () {
        BankAccount account =
                new BankAccount("123","Nguyen Van A",10);

        account.withdraw(20);
        assertEquals(10, account.getBalance());
    }

    @Test
    public void testCreateAccount_AccountNumberMustBeUnique () {

    }

    @Test
     public void testTransfer () {
        BankAccount senderAccount =
                new BankAccount("123","Nguyen Van A",100);
        BankAccount receiverAccount =
                new BankAccount("123","B",10);

        senderAccount.transfer(20, receiverAccount);
        assertEquals(80, senderAccount.getBalance());
        assertEquals(30, receiverAccount.getBalance());
    }

    @Test
    public void testInequality () {
        BankAccount a1 = new BankAccount("123","Nguyen Van A",100);
        BankAccount a2 = new BankAccount("13","B",10);

        assertNotEquals(a1, a2);
    }

    @Test
    public void testEquality_AccountsWithTheSameNumberAreEqual () {
        BankAccount a1 =
                new BankAccount("123","Nguyen Van A",100);
        BankAccount a2 =
                new BankAccount("123","Nguyen Van B",1000);

        assertEquals(a1, a2);
    }
}
