import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by dse on 12/7/2014.
 */
public class RoysDailyLogTest {
    @Test
    public void testDailyLogCodingAllDay() {
        RoysDailyLog log = new RoysDailyLog("CCCCC");

        assertTrue(log.codingAllDay);
        assertEquals(5, log.longestStreak);
        assertTrue(log.codingOverNight);
        assertTrue(log.codingSinceLastNight);
        assertEquals(5, log.firstStreak);
        assertEquals(5, log.lastStreak);
    }

    @Test
    public void testDailyLogCodingInMidday() {
        RoysDailyLog log = new RoysDailyLog("ECCCCS");

        assertFalse(log.codingAllDay);
        assertEquals(4, log.longestStreak);
        assertFalse(log.codingOverNight);
        assertFalse(log.codingSinceLastNight);
        assertEquals(0, log.firstStreak);
        assertEquals(0, log.lastStreak);
    }

    @Test
    public void testDailyLogCodingOvernight() {
        RoysDailyLog log = new RoysDailyLog("CCCSESECC");

        assertFalse(log.codingAllDay);
        assertEquals(3, log.longestStreak);
        assertTrue(log.codingOverNight);
        assertTrue(log.codingSinceLastNight);
        assertEquals(3, log.firstStreak);
        assertEquals(2, log.lastStreak);
    }
}
