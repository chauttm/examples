import java.util.Scanner;

/**
 * Created by dse on 12/7/2014.
 */
public class RoysDailyLog {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        input.nextLine();
        DayLog[] logs = new DayLog[N];
        for (int i = 0; i < N; i++) {
            logs[i] = new DayLog(input.nextLine());
        }

        int X = calculateLongestDailyStreak(logs);
        int Y = calculateLongestStreak(logs);
        System.out.print(X + " " + Y);
    }

    private static int calculateLongestDailyStreak(DayLog[] logs) {
        int max = logs[0].longestStreak;
        for (int i = 1; i < logs.length; i++) {
            max = Math.max(max, logs[i].longestStreak);
        }
        return max;
    }

    public static int calculateLongestStreak(DayLog[] logs) {
        int runningMax = 0, running = 0;
        boolean overnight = false;

        for (int i = 0; i < logs.length; i++) {
            runningMax = Math.max(runningMax, logs[i].longestStreak);
            if (overnight) {
                running += logs[i].firstStreak;
                if (!logs[i].codingAllDay) {
                    runningMax = Math.max(running, runningMax);
                    overnight = logs[i].codingOverNight;
                    running = logs[i].lastStreak;
                }
            } else {
                overnight = logs[i].codingOverNight;
                running = logs[i].lastStreak;
            }
        }
        runningMax = Math.max(running, runningMax);
        return runningMax;
    }
	
	static class DayLog {
	    public final boolean codingSinceLastNight;
		public final boolean codingAllDay;
		public final boolean codingOverNight;
		public final int firstStreak;
		public final int lastStreak;
		public final int longestStreak;

		public DayLog(String log) {
			longestStreak = calculateLongestStreak(log);
			codingAllDay = (longestStreak == log.length());
			codingSinceLastNight = (log.charAt(0) == 'C');
			codingOverNight = (log.charAt(log.length() - 1) == 'C');
			firstStreak = calculateFirstStreakOfDay(log);
			lastStreak = calculateLastStreakOfDay(log);
		}

		private int calculateLastStreakOfDay(String log) {
			int i = log.length() - 1;
			while (i >= 0 && log.charAt(i) == 'C') i--;
			return log.length() - i - 1;
		}

		private int calculateFirstStreakOfDay(String log) {
			int i = 0;
			while (i < log.length() && log.charAt(i) == 'C') i++;
			return i;
		}

		private int calculateLongestStreak(String log) {
			int max = 0, start = 0;
			for (int i = 0; i < log.length(); i++) {
				if (streakStartsAt(i, log)) {
					start = i;
				}
				if (streakEndsAt(i, log)) {
					max = Math.max(max, i - start + 1);
				}
			}
			return max;
		}

		private boolean streakStartsAt(int index, String log) {
			return log.charAt(index)=='C' &&
				   (index == 0 || log.charAt(index-1)!='C') ;
		}

		private boolean streakEndsAt(int index, String log) {
			return log.charAt(index)=='C' &&
					(index+1 == log.length() || log.charAt(index+1)!='C');
		}
	}
}

